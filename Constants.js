module.exports = {
    REGION: process.env.REGION,
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    HOSTED_ZONE_ID:process.env.HOSTED_ZONE_ID,
    DOMAIN_NAME:process.env.DOMAIN_NAME,
    PORT:process.env.PORT,
    DATABASE_URL:process.env.DATABASE_URL,
    DOMAIN_PATH:process.env.DOMAIN_PATH,
    SERVER_IP_ADDRESS:process.env.SERVER_IP_ADDRESS
}