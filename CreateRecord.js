const { Route53Client, ChangeResourceRecordSetsCommand } = require('@aws-sdk/client-route-53')
const dotenv = require('dotenv')
dotenv.config();
const Constants = require('./Constants')

const route53Client = new Route53Client({
    region: Constants.REGION,
    credentials: {
        accessKeyId: Constants.AWS_ACCESS_KEY_ID,
        secretAccessKey: Constants.AWS_SECRET_ACCESS_KEY
    }
})

const CreateRecord = async (slug) => {

    const params = {
        ChangeBatch: {
            Changes: [
                {
                    Action: 'UPSERT',
                    ResourceRecordSet: {
                        Name: `${slug}.${Constants.DOMAIN_NAME}`,
                        Type: 'A',
                        TTL: 300,
                        ResourceRecords: [
                            {
                                Value: `${Constants.SERVER_IP_ADDRESS}`
                            }
                        ]
                    }
                }
            ],
            Comment: `Creating an A record for ${slug}.${Constants.DOMAIN_NAME}`
        },
        HostedZoneId: Constants.HOSTED_ZONE_ID
    };

    const command = new ChangeResourceRecordSetsCommand(params);
    const data = await route53Client.send(command);
    return data;
}

module.exports = { CreateRecord };



